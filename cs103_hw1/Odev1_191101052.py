# Written by Aydin Mercan (191101052)
# Odev1_191101052's code is under public domain or MIT license if public domain is not recognized.

# Import the json module for reading the file
import json

# Load the data as processedJSON by opening the json file as a temporary statement (tmp)
with open("tobbData.json", "r") as tmp:
    processedJSON = json.load(tmp)

# Main Loop
while 1:
    try:
    # Input
        year = int(input('Enter Year - Yıl Giriniz: '))
        term = int(input('Enter Term - Dönem Giriniz: '))
    # Determine whether the input is legal or not
        if ( 0<year<5 and 0<term<4 ):
        # List indexing correction
            year = year-1
            term = term-1
        # Print cycle
            for n in range(0,len(processedJSON["year"][year]["term"][term]["code"])):
                print(processedJSON["year"][year]["term"][term]["code"][n], end=" ")
                print(processedJSON["year"][year]["term"][term]["name"][n], end=" ")
                print(processedJSON["year"][year]["term"][term]["credit"][n], end=" ")
                print(processedJSON["year"][year]["term"][term]["akts"][n])
        # Print-Exit for out of range integer
        else:
            print('Hazirlayan\nAydin Mercan - 191101052')
            exit(0)
    # Print-Exit for non-integer input
    except(ValueError, IndexError):
       print('Hazirlayan\nAydin Mercan - 191101052')
       exit(0)


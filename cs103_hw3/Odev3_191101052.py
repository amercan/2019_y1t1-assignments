# Written by Aydin Mercan - 191101052
# Under public domain or BSD 2-Clause Licence if public domain is not recognized.

import crypt

def dict(salt, cryptPass):
    # Initialize the dictionary file
    dictFile = open('dictionary.txt', 'r')
    # Test dictionary words with salt
    for word in dictFile.readlines():
        word = word.strip('\n')
        cryptWord = crypt.crypt(word,salt)
        # If a password is found, display it and return 0
        if cryptWord == cryptPass:
            print("Found Password: " + word)
            return 0
    # If a password isn't found, return 1
    return 1

def brute(salt, cryptPass):
    # Define password test array
    # Brute forcing takes a long time, replace the commented snipped with
    # uncommented one for speeding up evaluation time.
    #passwdEZ = '943000'
    #passwd = '00A'
    #passwdEZ = '0'
    #passwd = '0'
    cryptEval = ''

    while 1:
        if 0 < len(passwdEZ) < 8:
            cryptEval = crypt.crypt(passwdEZ, salt)
            passwdEZ = stepUp(passwdEZ, 0)
            if (cryptEval == cryptPass):
                print("Found Password: " + passwdEZ)
                return 0
        if 0 < len(passwd) < 5:
            cryptEval = crypt.crypt(passwd, salt)
            passwd = stepUp(passwd, 1)
            if (cryptEval == cryptPass):
                print("Found Password: " + passwd)
                return 0

def stepUp(passwd, state):
    if state:
        minorLim = ord('Z')+1
        majorLim = ord('z')+1

        # Switch to the next trial.
        # The if-else statement is for index errors when len(passwd) is 1.
        if len(passwd) == 1:
            passwd = chr(ord(passwd[0])+1)
        else:
            passwd = chr(ord(passwd[0])+1) + passwd[1:]

        # Fix the string such that it contains only a-z, A-Z & 1-9.
        for i in range(0,len(passwd)):
            if passwd[i] == '9':
                if i == len(passwd)-1:
                    passwd = passwd[:i] + 'A'
                else:
                    passwd = passwd[:i] + 'A' + passwd[i+1:]
            elif ord(passwd[i]) == minorLim:
                if i == len(passwd)-1:
                    passwd = passwd[:i] + 'a'
                else:
                    passwd = passwd[:i] + 'a' + passwd[i+1:]
            elif ord(passwd[i]) == majorLim:
                if i == len(passwd)-1:
                    passwd = passwd[:i] + '00'
                if i == len(passwd)-2:
                    passwd = passwd[:i] + '0' + chr(ord(passwd[i+1])+1)
                else:
                    passwd = passwd[:i] + '0' + chr(ord(passwd[i+1])+1) + passwd[i+2:]


    else:
        passwd = str(int(passwd)+1)

    return passwd


def main():
    # Define arrays for users, salted hashes and salts for flexibility
    user = []
    crypt = []
    salt = []
    # Setup value for dictionary attack failure
    dictFail = 0
    # Open & read the passowrds file
    passFile = open('passwords.txt')
    for line in passFile.readlines():
        # Get the users and salted hashes
        if ":" in line:
            user.append(line.split(':')[0])
            crypt.append(line.split(':')[1].strip('\n'))
        # Extract the salts which are the first two chars of crypt elements
        for i in crypt:
            salt = [j[0:2] for j in crypt]

    # Do the attack cycle for user at index i
    for i in range(0,len(user)):
        print("Cracking Password for: "+user[i])
        # Do the dictionary attack, 0 is for successful cracking and 1 is for no password found by the dictionary
        dictFail = dict(salt[i], crypt[i])
        # Result to bruteforcing if dictionary doesn't work
        if dictFail:
            brute(salt[i], crypt[i])
main() 

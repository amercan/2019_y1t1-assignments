/* Written by Aydin Mercan - 191101052 */

public class Decimal {

	public String revExp = "";
	private String strExp = "";

	public Decimal(String exp) {
		/* Store the expression as a string. */
		strExp = exp;
		/* Reverse the expression as this makes is easier to work with operators. */
		revExp = Decimal.revUp(exp);
	}

	/* Reverse the string. This is a method as it is done several times in codebase */
	public static String revUp(String enter) {
		String buffer = "";

		for (int i = enter.length()-1; i >= 0; i--)
			buffer = buffer + enter.charAt(i);

		return buffer;
	}

	/* Clean extra 0s */
	private static String cleanUp(String enter) {
		String tmp = enter;

		while (tmp.indexOf('0') == 0)
			tmp = tmp.substring(1);

		return tmp;
	}

	public Decimal add(Decimal other) {
		int currentDigit = 0, otherDigit = 0;
		String evalStr = "";
		Decimal eval;

		/* Add a buffer 0 for potential use. */
		other.revExp = other.revExp + "0";
		this.revExp = this.revExp + "0";

		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		/* Main addition part. i is for counting and j is for digit sum evaluation. */
		for (int i = 0, j = 0, carry = 0; i < this.revExp.length(); i++) {
			/* Get the digit number at index number i. */
			currentDigit = Character.getNumericValue(this.revExp.charAt(i));
			otherDigit = Character.getNumericValue(other.revExp.charAt(i));
			j = currentDigit + otherDigit + carry;

			/* Acts according to whether j is over 10 or not */
			if (j/10 == 1) {
				evalStr = (j%10) + evalStr;
				carry = 1;
			} else {
				evalStr = j + evalStr;
				carry = 0;
			}
		}

		/* Clean extra 0's and return. */
		eval = new Decimal(Decimal.cleanUp(evalStr));
		return eval;
	}

	public Decimal subtract(Decimal other) {
		int currentDigit = 0, otherDigit = 0;
		String evalStr = "";
		Decimal eval;

		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		/* Main subtraction part. i is for counting and j is for digit subtract evaluation. */
		for (int i = 0, j = 0, carry = 0; i < this.revExp.length(); i++) {
			currentDigit = Character.getNumericValue(this.revExp.charAt(i));
			otherDigit = Character.getNumericValue(other.revExp.charAt(i));
			j = currentDigit - otherDigit - carry;

			if (j < 0) {
				evalStr = (j+10) + evalStr;
				carry = 1;
			} else {
				evalStr = j + evalStr;
				carry = 0;
			}
		}

		/* Remove unnecessary 0's and return. */
		eval = new Decimal(this.cleanUp(evalStr));
		return eval;
	}


	public Decimal multiply(Decimal other) {
		Decimal one = new Decimal("1"), eval = new Decimal("0");

		for (Decimal counter = new Decimal("0"); !other.equals(counter); counter = counter.add(one))
			eval = eval.add(this);

		return eval;
	}

	public Decimal divide(Decimal other) {
		Decimal tmp = new Decimal("0"), one = new Decimal("1"), i = new Decimal("0"), eval;

		for (; this.greaterThan(tmp); i = i.add(one))
			tmp = tmp.add(other);

		eval = i;

		/* eval is one higher than its supposed to be unless tmp = this */
		if (!tmp.equals(this))
			eval = eval.subtract(one);

		return eval;
	}


	public void display() {
		System.out.println(strExp);
	}

	public boolean equals(Decimal other) {
		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		return this.revExp.equals(other.revExp);
	}


	public boolean greaterThan(Decimal other) {
		int currentDigit = 0, otherDigit = 0;
		boolean state = false;

		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		for (int i = this.revExp.length()-1; i >= 0; i--) {
			currentDigit = Character.getNumericValue(this.revExp.charAt(i));
			otherDigit = Character.getNumericValue(other.revExp.charAt(i));
			if (currentDigit > otherDigit) {
				state = true;
				break;
			}
			else if (otherDigit > currentDigit) {
				state = false;
				break;
			}
		}

		/* Return false if nothing is found */
		return state;

	}
}

/* Written by Aydin Mercan - 191101052 */

public class Binary {

	public String revExp = "";
	private String strExp = "";

	public Binary(String exp) {
		/* Store the expression as a string. */
		strExp = exp;
		/* Reverse the expression as this makes is easier to work with operators. */
		for (int i = exp.length()-1; i >= 0; i--)
			revExp = revExp + exp.charAt(i);
	}

	public Binary add(Binary other) {
		int currentBit = '0', otherBit = '0';
		String evalStr = "";
		Binary eval;

		/* Add a buffer 0 for potential use. */
		other.revExp = other.revExp + "0";
		this.revExp = this.revExp + "0";

		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		/* Main addition part. i is for counting and j is for bit evaluation. */
		for (int i = 0, j = 0, carry = 0; i < this.revExp.length(); i++) {
			currentBit = Character.getNumericValue(this.revExp.charAt(i));
			otherBit = Character.getNumericValue(other.revExp.charAt(i));
			j = currentBit + otherBit + carry;
			switch (j) {
				case 0:
					evalStr = "0" + evalStr;
					carry = 0;
					break;
				case 1:
					evalStr = "1" + evalStr;
					carry = 0;
					break;
				case 2:
					evalStr = "0" + evalStr;
					carry = 1;
					break;
				case 3:
					evalStr = "1" + evalStr;
					carry = 1;
					break;
			}
		}

		/* Remove unnecessary 0's and return */
		evalStr = evalStr.substring(evalStr.indexOf('1'));
		eval = new Binary(evalStr);
		return eval;

	}

	public Binary subtract(Binary other) {
		int currentBit = '0', otherBit = '0';
		String evalStr = "";
		Binary eval;

		/* Make the numbers equal in character length by adding 0's for consistency */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		/* Main subtraction part. i is for counting and j is for bit evaluation. */
		for (int i = 0, j = 0, carry = 0; i < this.revExp.length(); i++) {
			currentBit = Character.getNumericValue(this.revExp.charAt(i));
			otherBit = Character.getNumericValue(other.revExp.charAt(i));
			j = currentBit - otherBit - carry;
			switch (j) {
				case -2:
					evalStr = "0" + evalStr;
					carry = 1;
					break;
				case -1:
					evalStr = "1" + evalStr;
					carry = 1;
					break;
				case 0:
					evalStr = "0" + evalStr;
					carry = 0;
					break;
				case 1:
					evalStr = "1" + evalStr;
					carry = 0;
					break;
			}
		}

		/* Remove unnecessary 0's and return. */
		evalStr = evalStr.substring(evalStr.indexOf('1'));
		eval = new Binary(evalStr);
		return eval;

	}

	public Binary multiply(Binary other) {
		Binary one = new Binary("1"), eval = new Binary("0");

		for (Binary counter = new Binary("0"); !other.equals(counter); counter = counter.add(one))
			eval = eval.add(this);

		return eval;
	}

	public Binary divide(Binary other) {
		Binary tmp = new Binary("0"), one = new Binary("1"), i = new Binary("0"), eval;

		for (; this.greaterThan(tmp); i = i.add(one))
			tmp = tmp.add(other);

		eval = i;

		/* eval is one higher than its supposed to be unless tmp = this */
		if (!tmp.equals(this))
			eval = eval.subtract(one);

		return eval;
	}


	public void display() {
		System.out.println(strExp);
	}

	public boolean equals(Binary other) {

		/* Make the numbers equal in character length by adding 0's for consistency */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";


		return this.revExp.equals(other.revExp);
	}

	public boolean greaterThan(Binary other) {
		int currentDigit = 0, otherDigit = 0;
		boolean state = false;

		/* Make the numbers equal in character length by adding 0's for consistency. */
		while (this.revExp.length() < other.revExp.length())
			this.revExp = this.revExp + "0";
		while (other.revExp.length() < this.revExp.length())
			other.revExp = other.revExp + "0";

		for (int i = this.revExp.length()-1; i >= 0; i--) {
			currentDigit = Character.getNumericValue(this.revExp.charAt(i));
			otherDigit = Character.getNumericValue(other.revExp.charAt(i));
			if (currentDigit > otherDigit) {
				state = true;
				break;
			}
			else if (otherDigit > currentDigit) {
				state = false;
				break;
			}
		}

		/* Return false if nothing is found */
		return state;

	}


}

# Written by Aydin Mercan
# Available under BSD-2-Clause License

import sqlite3
from sqlite3 import Error


# Create the lists here because the database is created inside the memory only
ogrenci_list = (
        (191101, 'Alice Lastname', 'alice@example.net'),
        (191102, 'Bob Anotherlastname', 'bob@crypto.strong'),
        (191103, 'Run Bsd', 'openbsd@freebsd.bsd'),
        (191104, 'Foo Bar', 'foo@bar.org'),
        (191105, 'John Khan', 'johnkhan@provite.net'),
        (191106, 'Johnatthan Joestar', 'johnattan@joestar.gov'),
        (191107, 'Puffy SSH', 'openssh@rocks.net'),
        (191108, 'Caesar Cipher', 'crypto@outdated.xyz'),
        (191109, 'John Doe', 'johndoe@popular.mail'),
        (191110, 'Mohammad Avdol', 'yes@i.am')
)

dersBilgi_list = (
        ('BIL103', 'Bilgisayar Bilimlerine Giris - I', 4)
)

ders_list = (
        ('BIL103', '1. Donem, 2019', '191101', 'BA'),
        ('BIL103', '1. Donem, 2019', '191102', 'CC'),
        ('BIL103', '1. Donem, 2019', '191103', 'AA'),
        ('BIL103', '1. Donem, 2019', '191104', 'CD'),
        ('BIL103', '2. Donem, 2018', '191105', 'AA'),
        ('BIL103', '1. Donem, 2019', '191106', 'BB')
)


# Create the DB in memory, relations and commit
db_mgmt = sqlite3.connect(':memory:')
db_cursor = db_mgmt.cursor()
db_cursor.execute('''CREATE TABLE DersBilgisi(DersKodu TEXT, DersIsmi TEXT, DersKredisi INTEGER)''')
db_cursor.execute('''CREATE TABLE DersListesi(DersKodu TEXT, AcilanDonem TEXT, OgrenciNo INTEGER, HarfNotu TEXT)''')
db_cursor.execute('''CREATE TABLE Ogrenci(OgrenciNo INTEGER, OgrenciIsmi TEXT, OgrenciEposta TEXT)''')
db_mgmt.commit()

# Fill inside the relations and commit again
db_cursor.executemany('''INSERT INTO Ogrenci VALUES(?, ?, ?)''', ogrenci_list)
db_cursor.execute('''INSERT INTO DersBilgisi VALUES(?, ?, ?)''', dersBilgi_list)
db_cursor.executemany('''INSERT INTO DersListesi VALUES(?, ?, ?, ?)''', ders_list)
db_mgmt.commit()

# Select the AA grades and print their name/email
db_cursor.execute(''' SELECT Ogrenci.OgrenciIsmi, Ogrenci.OgrenciEposta\
                      FROM Ogrenci\
                      INNER JOIN DersListesi\
                      ON Ogrenci.OgrenciNo = DersListesi.OgrenciNo
                      WHERE DersListesi.HarfNotu = 'AA'; ''')

eval_print = db_cursor.fetchall()
for i in range(len(eval_print)):
    print(str(eval_print[i]))

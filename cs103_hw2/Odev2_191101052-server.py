import json
import socket
import time

# Load the data as processedJSON by opening the json file as a temporary statement (tmp)
with open("tobbData.json", "r") as tmp:
    processedJSON = json.load(tmp)

# Server initialization, listen to only one connection at a time
tcpSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpSocket.bind(('',12000))
tcpSocket.listen(1)
print("Server is ready.")

while 1:
    try:
        inSocket, addr = tcpSocket.accept()
        # Recieve, decode and cast year
        yearIn = inSocket.recv(1024)
        yearDecode = yearIn.decode("utf-8")
        year = int(yearDecode)
        # Recieve, decode and cast term
        termIn = inSocket.recv(1024)
        termDecode = termIn.decode("utf-8")
        term = int(termDecode)
        # Prepare SIGEXIT calls 0 = successful, 1 = error
        sig0 = bytes('SIGKILL0', "utf-8")
        sig1 = bytes('SIGKILL1', "utf-8")
        # Determine whether the input is legal or not
        if ( 0<year<5 and 0<term<4 ):
        # List indexing correction
            year = year-1
            term = term-1
        # send length
            #length = int(processedJSON["year"][year]["term"][term]["code"])
            #lengthEncoded = bytes(length, "utf-8")
           # inSocket.send(length)
            for n in range(0,len(processedJSON["year"][year]["term"][term]["code"])):
                show1 = str(processedJSON["year"][year]["term"][term]["code"][n])
                data1 = bytes(show1, "utf-8")
                inSocket.send(data1)

                show2 = str(processedJSON["year"][year]["term"][term]["name"][n])
                data2 = bytes(show2, "utf-8")
                inSocket.send(data2)

                show3 = str(processedJSON["year"][year]["term"][term]["credit"][n])
                data3 = bytes(show3, "utf-8")
                inSocket.send(data3)

                show4 = str(processedJSON["year"][year]["term"][term]["akts"][n])
                data4 = bytes(show4, "utf-8")
                inSocket.send(data4)
            inSocket.send(sig0)
    
    except(ValueError, IndexError):
        inSocket.send(sig1)

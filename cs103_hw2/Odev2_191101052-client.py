import socket
serverName = input('Enter IP Address: ')
serverPort = 12000

# Connect to server
clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket.connect((serverName, serverPort))
# Take year input, encode and send it to server
yearString = input('Enter Year - Yil Giriniz: ')
yearEncoded = bytes(yearString, "utf-8")
clientSocket.send(yearEncoded)
# Take term input, encode and send it to server
termString = input('Enter Term - Donem Giriniz: ')
termEncoded = bytes(termString, "utf-8")
clientSocket.send(termEncoded)
# Take and display input
#lengthEncoded = clientSocket.recv(1024)
#lengthDecoded = lengthEncoded.decode("utf-8")
# lengthOf = int(len(lengthDecoded))
#for n in range(0, lengthOf):
dataEncoded = clientSocket.recv(1024)
dataDecoded = dataEncoded.decode("utf-8")
print(dataDecoded, end=" ")
#    if (dataDecoded == 'SIGEXIT0'):
#       break
if (dataDecoded == 'SIGEXIT1'):
    print('\nHazirlayan\nAydin Mercan - 191101052')
#       break
clientSocket.close()


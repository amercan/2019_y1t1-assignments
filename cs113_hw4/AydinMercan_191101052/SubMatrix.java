/*
 * Written by Aydin Mercan - 191101052
 * The following code is under BSD-2-Clause License.
 */
import java.util.Arrays;

public class SubMatrix {

	/*
	 * The coordinate format is as follows:
	 * {row, column, direction} where
	 * -1 <-----direction-----> +1
	 */
	/* Not allowed to use generics means I have to write this a second time for strings. */
	public static int[] stepUp(int[] coord, String[][] mtx) {
		/* Jump to the lower row and switch direction */
		if (coord[1] == mtx[0].length-1 && coord[2] == 1 || coord[1] == 0 && coord[2] == -1) { 
			coord[0] += +1;
			coord[2] *= -1;
		} else {
			coord[1] += coord[2];
		}
		return coord;
	}
	/* Computes the result of an operation applied to the elements of a given matrix. */
	public static int elementOp(int[][] mtx, char op) {
		int eval = 0;
		if (op == '*' || op == '/')
			eval = 1;
		else if (op == '+' || op == '-')
			eval = 0;

		for (int i = 0; i < mtx.length; i++)
			for (int j = 0; j < mtx[i].length; j++)
				switch (op) {
				case '+':
					eval += mtx[i][j];
					break;
				case '-':
					eval -= mtx[i][j];
					break;
				case '*': 
					eval *= mtx[i][j];
					break;
				case '/':
					eval /= mtx[i][j];
				}
		return eval;
	}

	/* Find the boolean according to its operator. */
	public static boolean booleanOp(char op, int inMtx, int inVal) {
		if (op == '=' && inMtx == inVal)
			return true;
		else if (op == '>' && inMtx > inVal)
			return true;
		else if (op == '<' && inMtx < inVal)
			return true;
		return false;
	}

	/* Trim the matrix by one row/column. */
	public static int[][] trimMatrix(int[][] mtx, char dir) {
		int[][] eval = new int[0][0];
		switch (dir) {
		/* Trim one row */
		case 'r':
			eval = new int[mtx.length-1][mtx[0].length];
			for (int i = 0; i < eval.length; i++)
				eval[i] = mtx[i];
			break;
		/* Trim one column */
		case 'c':
			eval = new int[mtx.length][mtx[0].length-1];
			for (int i = 0; i < eval.length; i++)
				for (int j = 0; j < eval[0].length; j++)
					eval[i][j] = mtx[i][j];
			break;
		}
		return eval;
	}

	/* Trim the matrix by one row/column. */
	public static String[][] trimMatrix(String[][] mtx, char dir) {
		String[][] eval = new String[0][0];
		switch (dir) {
		/* Trim one row */
		case 'r':
			eval = new String[mtx.length-1][mtx[0].length];
			for (int i = 0; i < eval.length; i++)
				eval[i] = mtx[i];
			break;
		/* Trim one column */
		case 'c':
			eval = new String[mtx.length][mtx[0].length-1];
			for (int i = 0; i < eval.length; i++)
				for (int j = 0; j < eval[0].length; j++)
					eval[i][j] = mtx[i][j];
			break;
		}
		return eval;
	}

	/* Create a submatrix such that a given coordinate is the upleft most point */
	public static int[][] createSubMatrix(int[][] mtx, int[] coord) {
		int[][] eval = new int[mtx.length-coord[0]][mtx[0].length-coord[1]];
		for (int i = 0; i < eval.length; i++)
			for (int j = 0; j < eval[0].length; j++)
				eval[i][j] = mtx[coord[0]+i][coord[1]+j];
		return eval;
	}

	public static String[][] createSubMatrix(String[][] mtx, int[] coord) {
		String[][] eval = new String[mtx.length-coord[0]][mtx[0].length-coord[1]];
		for (int i = 0; i < eval.length; i++)
			for (int j = 0; j < eval[0].length; j++)
				eval[i][j] = mtx[coord[0]+i][coord[1]+j];
		return eval;
	}

	/* Condition tests */
	public static void condTest(int[][] mtx, int[][] M, int[] coord, char op, char condition, int val) {
		if (SubMatrix.booleanOp(condition, SubMatrix.elementOp(mtx, op), val)) {
			System.out.print("("+coord[1]+","+coord[0]+"), ");
			System.out.print("("+(M[0].length-1)+","+(M.length-1)+")");
			System.out.println();
		}
	}

	public static void condTest(String[][] mtx, String[][] M, int[] coord, int val) {
		if (Arrays.toString(mtx).length() == val) {
			System.out.print("("+coord[1]+","+coord[0]+"), ");
			System.out.print("("+(M[0].length-1)+","+(M.length-1)+")");
			System.out.println();
		}
	}

	public void findSubMatrix(int[][] M, char op, char condition, int val) {
		int[][] eval = new int[0][0], buffer = new int[0][0];
		int[] coord = new int[2];
		/* 
		 * i goes in a matrix like such
		 * 0 1 2  3
		 * 4 5 6  7
		 * 8 9 10 11
		 */
		for (int i = 0; i < M.length*M[0].length; i++) {
			/* Update the coordinate */
			coord[0] = i/M[0].length;
			coord[1] = i%M[0].length;
			/* Create the largest submatrix and test */
			eval = SubMatrix.createSubMatrix(M, coord);
			SubMatrix.condTest(eval, M, coord, op, condition, val);
			/* Start trimming and testing */
			while (eval.length > 1) {
				eval = SubMatrix.trimMatrix(eval, 'r');
				SubMatrix.condTest(eval, M, coord, op, condition, val);
				buffer = SubMatrix.trimMatrix(eval, 'c');
				while (buffer[0].length > 1) {
					SubMatrix.condTest(buffer, M, coord, op, condition, val);
					buffer = SubMatrix.trimMatrix(buffer, 'c');
				}
				SubMatrix.condTest(buffer, M, coord, op, condition, val);
			}
			SubMatrix.condTest(eval, M, coord, op, condition, val);

		}

	}

	public void findSubMatrix(String[][] S, int val) {
		String[][] eval = new String[0][0], buffer = new String[0][0];
		int[] coord = new int[2];
		/* 
		 * i goes in a matrix like such
		 * 0 1 2  3
		 * 4 5 6  7
		 * 8 9 10 11
		 */
		for (int i = 0; i < S.length*S[0].length; i++) {
			/* Update the coordinate */
			coord[0] = i/S[0].length;
			coord[1] = i%S[0].length;
			/* Create the largest submatrix and test */
			eval = SubMatrix.createSubMatrix(S, coord);
			SubMatrix.condTest(eval, S, coord, val);
			/* Start trimming and testing */
			while (eval.length > 1) {
				eval = SubMatrix.trimMatrix(eval, 'r');
				SubMatrix.condTest(eval, S, coord, val);
				buffer = SubMatrix.trimMatrix(eval, 'c');
				while (buffer[0].length > 1) {
					SubMatrix.condTest(buffer, S, coord, val);
					buffer = SubMatrix.trimMatrix(buffer, 'c');
				}
				SubMatrix.condTest(buffer, S, coord, val);
			}

			SubMatrix.condTest(eval, S, coord, val);

		}

	}

}

/* Written by Aydin Mercan - 191101052 */
/* Under public domain or BSD-2-Clause License if public domain is not recognized. */
import java.util.Arrays;

public class Multiset {

	/* This method counts the amount of state present in compArr. */
	public static int inCount (int state, int[] compArr) {
		int count = 0;

		for (int i = 0; i < compArr.length; i++)
			if (compArr[i] == state)
				count++;

		return count;
	}

        /* Adds indexOf functionality for integer arrays. */
	public static int indexOf (int query, int[] arr) {
	        int eval = 0;
	        while (arr[eval] != query)
	                eval++;
	        return eval;
        }

        /* Figures out. */
        public static boolean equals(int[] A, int[] B) {
                for (int i = 0; i < A.length; i++)
                        if (A[i] != B[i])
                                return false;
                return true;
        }

        /* Check whether an int is an element of the given array or not. */
	public static boolean elementOf(int query, int[] arr) {
	        for (int i = 0; i < arr.length; i++)
	                if (arr[i] == query)
	                        return true;
	        return false;
        }

        /* Trunctuate the superset into a normal one. */
        public static int[] toSet(int[] arr) {
                int eval[] = new int[0];
                /* Only adds non-duplicate elenments of arr to eval. */
                for (int i = 0; i < arr.length; i++)
                        if (!Multiset.elementOf(arr[i], eval))
                                eval = Multiset.repAdd(arr[i], 1, eval);
                Arrays.sort(eval);
                return eval;

        }

        /* Addition of in repeated count-times array no matter the size. */
        public static int[] repAdd(int in, int count, int[] arr) {
                int eval[] = new int[arr.length+count];

                /* Safely copy the array for evaluation. */
                for (int i = 0; i < arr.length; i++)
                        eval[i] = arr[i]; 

                /* Addition cycle. */
                for (int i = 0; i < count; i++)
                        eval[arr.length+i] = in;

                Arrays.sort(eval);
                return eval;
        }

	public static int[] intersection(int[] A, int[] B) {
		int eval[] = new int[0];
		int tmpA, tmpB;

		for (int i = 0; i < A.length; i++) {
		        tmpA = Multiset.inCount(A[i], A);
		        tmpB = Multiset.inCount(A[i], B);
		        if (tmpA < tmpB && !Multiset.elementOf(A[i], eval))
		                eval = Multiset.repAdd(A[i], tmpA, eval);
                        else if (!Multiset.elementOf(A[i], eval))
                                eval = Multiset.repAdd(A[i], tmpB, eval);
                }
                
                Arrays.sort(eval);
                return eval;

        }

        public static int[] difference(int[] A, int[] B) {
                int eval[] = new int[0];
                int tmpA, tmpB;

                for (int i = 0; i < A.length; i++)
                        if (!Multiset.elementOf(A[i],B))
                                eval = Multiset.repAdd(A[i], 1, eval);
                Arrays.sort(eval);
                return eval;
        }

        public static int[] union(int[] A, int[] B) {
                int eval[] = new int[0], buffer[];
                int tmpA, tmpB;
		/* Add most except B difference A. */
                for (int i = 0; i < A.length; i++) {
		        tmpA = Multiset.inCount(A[i], A);
		        tmpB = Multiset.inCount(A[i], B);
		        if (tmpA > tmpB && !Multiset.elementOf(A[i], eval))
		                eval = Multiset.repAdd(A[i], tmpA, eval);
                        else if (!Multiset.elementOf(A[i], eval))
                                eval = Multiset.repAdd(A[i], tmpB, eval);
                }
                        /* Add B difference A. */
                        for (int i = 0; i < Multiset.difference(B,A).length; i++)
                                eval = Multiset.repAdd(Multiset.difference(B,A)[i], 1, eval);

                Arrays.sort(eval);
                return eval;
        }

        /* Display the (super)set. */
        public static void display(int[] arr) {
                for (int i = 0; i < arr.length; i++)
                        System.out.print(arr[i]+" ");
                System.out.println();

        }
        
        /* Calculate the sum of the array*/
        public static int setSum(int[] arr) {
                int eval = 0;
                for (int i = 0; i < arr.length; i++)
                        eval += arr[i];
                return eval;
        }

        /* Fix the set so index numbers are ordered properly */
        public static int[] setFix(int[] arr) {
                boolean fixed = false;
                if (arr.length == 1)
                        return arr;

                do {
                        fixed = true;
                        for (int i = 0; i < arr.length-1; i++) {
                                if (arr[i] >= arr[i+1]) {
                                        fixed = false;
                                        if (i == 0) {
                                                arr[i] = 0;
                                                arr[i+1]++;
                                        } else {
                                                arr[i] = arr[i-1]+1;
                                                arr[i+1]++;
                                        }
                                }
                        }
                } while (!fixed);

                return arr;
        }

        public static void subsetSum(int[] A, int K) {
                int buffer[] = new int[Multiset.toSet(A).length];
                int eval[] = new int[0];
                int disp[] = new int[0];
                int endState[] = new int[0];
                /* Safely copy the normalized superset for evaluation. */
                for (int i = 0; i < Multiset.toSet(A).length; i++)
                        buffer[i] = Multiset.toSet(A)[i]; 

                Arrays.sort(buffer);
                
                /*
                 * len is the length of the subset.
                 * eval is the index of the subset nums in buffer
                 */
                for (int len = 1; len < A.length; len++) {
                        /* Reset variables from previous iteration. */
                        eval = new int[len];
                        endState = new int[len];
                        /* Compute starting and ending position. */
                        for (int i = 0; i < len; i++) {
                                eval[i] = i+1;
                                endState[len-i-1] = buffer.length-i-1;
                        }

                        /* Sum, print and next step cycle. */
                        while (!Multiset.equals(eval, endState)) {
                                eval = Multiset.setFix(eval);
                                if (Multiset.setSum(eval) == K) {
                                        disp = new int[0];
                                        for (int i = 0; i < eval.length; i++)
                                                disp = Multiset.repAdd(buffer[eval[i]]-1, 1 , disp);
                                        if (!Multiset.elementOf(0, disp))
                                                Multiset.display(disp);
                                        eval[0]++;
                                } else {
                                        eval[0]++;
                                }
                        }
                }

        }
}

import java.util.Scanner;

public class AydinMercan_191101052 {
	public static void main(String[] args) {
		int numSum = 0, numMul = 0, numDiv = 0, numMod = 0,
		    firInt = 0, secInt = 0, resInt = 0, tmpInt = 0;
		char tmpChr = 'b', opChr = 'b';
		boolean noOperator = false, tmpBool = false, zeroErr = false;
		String tmpStr = "", evalStr = "";

		/* Initialize the scanner. */
		Scanner keyIn = new Scanner(System.in);

		/* Take input. */
		String inExp = keyIn.nextLine();

		/* This do-while loop will continue the evaluation system until there are no operators left. */
		do{
			/* Convert double negatives to positive if present. Makes it easier to parse. */
			while (inExp.contains("--"))
				inExp = inExp.substring(0,inExp.indexOf("--"))+"+"+inExp.substring(inExp.indexOf("--")+2);

			/* Add outermost parantheses if not present. */
			if (inExp.charAt(0) != '(')
				inExp = "("+inExp;
			if (inExp.lastIndexOf(')') != inExp.length()-1)
					inExp = inExp+")";
			/* Determine the location of the next operation. */
			tmpStr = inExp.substring(inExp.lastIndexOf('(')+1);
			evalStr = tmpStr.substring(0,tmpStr.indexOf(')'));

			/* Makes it such that a-b is a+-b. */
			for (int i = 0,j = 0; i<evalStr.length(); i++) {
				tmpStr = evalStr.substring(i);
				if (tmpStr.indexOf('-') == 0 || !tmpStr.contains("-"))
					/* Break the switch if the - is at the beginning of tmpStr */
					tmpChr = 'b';
				else
					tmpChr = tmpStr.charAt(tmpStr.indexOf('-')-1);
				/* Switch to evaluate whether its available for correction or not */
				switch (tmpChr) {
					case 'b':
					case '*':
					case '/':
					case '%':
					case '+':
					case '(':
						break;
					default:
						j = tmpStr.indexOf('-');
						evalStr = evalStr.substring(0,i+j)+"+-"+evalStr.substring(i+j+1);
				}
			}

			/* Determine the operation that will be done. */
			numSum = evalStr.indexOf('+');
			numDiv = evalStr.indexOf('/');
			numMul = evalStr.indexOf('*');
			numMod = evalStr.indexOf('%');

			if (numMod == -1 && numMul == -1 && numDiv == -1 && numSum == -1) {
				/* 'b' is the break string for no operation in evalStr */
				opChr = 'b';
			}
			else if ((numMod < numMul || numMul == -1) && (numMod < numDiv || numDiv == -1) && numMod != -1)
				opChr = '%';
			else if ((numMul < numMod || numMod == -1) && (numMul < numDiv || numDiv == -1) && numMul != -1)
				opChr = '*';
			else if ((numDiv < numMul || numMul == -1) && (numDiv < numMod || numMod == -1) && numDiv != -1)
				opChr = '/';
			else if (numSum != -1)
				opChr = '+';
			else
				opChr = 'b';

			/* Isolate the first number as a string. */
			tmpStr = "";
			for (int i = evalStr.indexOf(opChr)-1; i>=0; i--) {
				tmpChr = evalStr.charAt(i);
				if (tmpChr == '+' || tmpChr == '*' || tmpChr == '/' || tmpChr == '%')
					break;
				else
					tmpStr += tmpChr;
			}

			/* Parse the first number */
			firInt = 0;
			for (int i = 0, n = 1; i < tmpStr.length(); i++, n*=10) {
				tmpChr = tmpStr.charAt(i);
				switch (tmpChr) {
				case '-':
					firInt *= -1;
					break;
				case '1':
					firInt += 1*n;
					break;
				case '2':
					firInt += 2*n;
					break;
				case '3':
					firInt += 3*n;
					break;
				case '4':
					firInt += 4*n;
					break;
				case '5':
					firInt += 5*n;
					break;
				case '6':
					firInt += 6*n;
					break;
				case '7':
					firInt += 7*n;
					break;
				case '8':
					firInt += 8*n;
					break;
                                case '9':
					firInt += 9*n;
					break;
				case '0':
					break;
				default:
					i = -1;
					break;
				}
			}

			/* Isolate the second number. */
			tmpStr = "";
			for (int i = evalStr.indexOf(opChr)+1; i < evalStr.length(); i++) {
				tmpChr = evalStr.charAt(i);
				if (tmpChr == '+' || tmpChr == '*' || tmpChr == '/' || tmpChr == '%')
					break;
				else
					tmpStr += tmpChr;
			}


			/* Parse the second number. This one isn't reversed so I made it start from the end to the beginning. */
			secInt = 0;
			for (int i = tmpStr.length()-1, n = 1; i >= 0; i--, n*=10) {
				tmpChr = tmpStr.charAt(i);
				switch (tmpChr) {
				case '-':
					secInt *= -1;
					break;
				case '1':
					secInt += 1*n;
					break;
				case '2':
					secInt += 2*n;
					break;
				case '3':
					secInt += 3*n;
					break;
				case '4':
					secInt += 4*n;
					break;
				case '5':
					secInt += 5*n;
					break;
				case '6':
					secInt += 6*n;
					break;
				case '7':
					secInt += 7*n;
					break;
				case '8':
					secInt += 8*n;
					break;
				case '9':
					secInt += 9*n;
					break;
				case '0':
					break;
				default:
					i = -1;
					break;
				}
			}

			/* Do the operation */
			switch (opChr) {
			case '+':
				resInt = firInt+secInt;
				break;
			case '*':
				resInt = firInt*secInt;
				break;
			case '/':
				/* Handle division by zero errors as required */
				if (secInt == 0) {
					zeroErr = true;
					noOperator = true;
				}
				else {
					resInt = firInt/secInt;
				}
				break;
			case '%':
				resInt = firInt%secInt;
				break;
			}

			/* Insert the evaluation result back to the initial expression */
			inExp = inExp.replace(""+firInt+opChr+secInt,""+resInt);

			/* Check if there are any uncorrected substractions created */
			for (int i = 0, j = 0; i<inExp.length(); i++) {
				tmpStr = inExp.substring(i);
				if (tmpStr.indexOf('-') == 0 || !tmpStr.contains("-"))
					/* Break the switch if the - is at the beginning of tmpStr */
					tmpChr = 'b';
				else
					tmpChr = tmpStr.charAt(tmpStr.indexOf('-')-1);
				/* Switch to evaluate whether its available for correction or not */
				switch (tmpChr) {
					case 'b':
					case '*':
					case '/':
					case '%':
					case '+':
					case '(':
						break;
					default:
						j = tmpStr.indexOf('-');
						inExp = inExp.substring(0,i+j)+"+-"+inExp.substring(i+j+1);
				}

			}

			/* Clean leftover parantheses */
			for (int i = 0; i<inExp.length(); i++) {
				tmpStr = "+*/%";
				tmpChr = inExp.charAt(i);
				if (tmpChr == '(') {
					tmpBool = true;
					tmpInt = i;
				}
				else if (tmpBool && tmpStr.contains(""+tmpChr)){
					tmpBool = false;
				}
				else if (tmpChr == ')' && tmpBool) {
					inExp = inExp.substring(0,tmpInt)+resInt+inExp.substring(i+1);
					tmpBool = false;
				}
			}

			/* Do not contiue the do-while loop if there are no operations left. */
			if (!inExp.contains("/") && !inExp.contains("*") && !inExp.contains("%") && !inExp.contains("+"))
				noOperator = true;

		} while (!noOperator);

		/* Print the result. */
		if (zeroErr)
			System.out.println("ERROR: Division by Zero");
		else
			System.out.println(resInt);
	}
}
